Nibbleblog - stripped-down version without cookies, external fonts or files and
no JavaScript in the frontend.

Version >=sd.1.1 ready for PHP 7.4 and 8.0. version sd.1.3 ready for PHP 8.1 and
and sd.1.6 for >=8.2.

Changelog:

- fixed bug in db_pages.class.php line 502
- removed JS based plugins analytics and mathjax
- removed external fonts from .css files in all themes
- removed external comments from Facebook and Disqus
- removed external avatar picture for comments
- removed JS based themes note-2 and techie
- removed JS from all themes
- updated plain social-media links
- no cookie session in frontend
- mod for values for items selection in backend
- added post-by-year plugin
- added pager for older posts
- reverted older/newer link arrow
- added Colored theme with by year plugin and pager for older posts
- fixes for PHP 7.4 and PHP 8.0
- fixed missing normalize.css and favicon.ico in admin/templates/default/
- removed version.php query
- fix for deprecated strftime in date.class.php line 66
- surpressed dep warnings in nbxml.class.php
- replaced deprecated (PHP v9) utf8_encode/decode with mb_convert_encoding

[Nibbleblog](http://www.nibbleblog.com/)
================================================

License
-------
Nibbleblog is opensource software licensed under the [GPL v3](http://www.gnu.org/licenses/gpl-3.0.txt)
