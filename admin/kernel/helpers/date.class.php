<?php

/*
 * Nibbleblog -
 * http://www.nibbleblog.com
 * Author Diego Najar

 * All Nibbleblog code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
*/

class Date {

	public static function set_locale($string)
	{
		if(setlocale(LC_ALL,$string.'.UTF-8')!==false)
			return true;

		if(setlocale(LC_ALL,$string.'.UTF8')!==false)
			return true;

		return setlocale(LC_ALL,$string);
	}

	public static function set_timezone($string)
	{
		return(date_default_timezone_set($string));
	}

	// Return array('Africa/Abidjan'=>'Africa/Abidjan (GMT+0)', ..., 'Pacific/Wallis'=>'Pacific/Wallis (GMT+12)');
	// PHP supported list. http://php.net/manual/en/timezones.php
	public static function get_timezones()
	{
		$tmp = array();

		$timezone_identifiers_list = timezone_identifiers_list();

		foreach($timezone_identifiers_list as $timezone_identifier)
		{
			$date_time_zone = new DateTimeZone($timezone_identifier);
			$date_time = new DateTime('now', $date_time_zone);

			$hours = floor($date_time_zone->getOffset($date_time) / 3600);
			$mins = floor(($date_time_zone->getOffset($date_time) - ($hours*3600)) / 60);

			$hours = 'GMT' . ($hours < 0 ? $hours : '+'.$hours);
			$mins = ($mins > 0 ? $mins : '0'.$mins);

			$text = str_replace("_"," ",$timezone_identifier);

			$tmp[$timezone_identifier]=$text.' ('.$hours.':'.$mins.')';
		}

		return($tmp);
	}

	// Time GMT
	public static function unixstamp()
	{
		return( time() );
	}

	public static function transformformat($format)
  {
    // replace from `strftime` format to `IntlDateFormatter` pattern.
    // based on German locale.
    $replaces = [
        '%a' => 'ccc',
        '%A' => 'cccc',
        '%d' => 'dd',
        '%e' => 'd',
        '%j' => 'D',
        '%u' => 'e',// not 100% correct
        '%w' => 'c',// not 100% correct
        '%U' => 'w',
        '%V' => 'ww',// not 100% correct
        '%W' => 'w',// not 100% correct
        '%b' => 'LLL',
        '%B' => 'LLLL',
        '%h' => 'LLL',// alias of %b
        '%m' => 'LL',
        '%C' => '\'{{century}}\'',// no replace for this
        '%g' => 'yy',// no replace for this
        '%G' => 'Y',// not 100% correct
        '%y' => 'yy',
        '%Y' => 'yyyy',
        '%H' => 'HH',
        '%k' => 'H',
        '%I' => 'hh',
        '%l' => 'h',
        '%M' => 'mm',
        '%p' => 'a',
        '%P' => 'a',// no replace for this
        '%r' => 'hh:mm:ss a',
        '%R' => 'HH:mm',
        '%S' => 'ss',
        '%T' => 'HH:mm:ss',
        '%X' => 'HH:mm:ss',// no replace for this
        '%z' => 'ZZ',
        '%Z' => 'zzz',// no replace for this
        '%c' => 'ccc d LLL YYYY HH:mm:ss zzz',// Buddhist era not converted.
        '%D' => 'MM/dd/yy',
        '%F' => 'yyyy-MM-dd',
        '%s' => '\'{{timestamp}}\'',// no replace for this
        '%x' => 'd.MM.yyyy',// Buddhist era not converted.
        '%n' => "\n",
        '%t' => "\t",
        '%%' => '%',
    ];

    $pattern = $format;
    // replace 1 single quote that is not following visible character or single quote and not follow by single quote or word or number.
    // example: '
    // replace with 2 single quotes. example: ''
    $pattern = preg_replace('/(?<![\'\S])(\')(?![\'\w])/u', "'$1", $pattern);
    // replace 1 single quote that is not following visible character or single quote and follow by word.
    // example: 'xx
    // replace with 2 single quotes. example: ''xx
    $pattern = preg_replace('/(?<![\'\S])(\')(\w+)/u', "'$1$2", $pattern);
    // replace 1 single quote that is following word (a-z 0-9) and not follow by single quote.
    // example: xx'
    // replace with 2 single quotes. example: xx''
    $pattern = preg_replace('/([\w]+)(\')(?!\')/u', "$1'$2", $pattern);
    // replace a-z (include upper case) that is not following %. example xxx.
    // replace with wrap single quote. example: 'xxx'.
    $pattern = preg_replace('/(?<![%a-zA-Z])([a-zA-Z]+)/u', "'$1$2'", $pattern);

    // escape %%x with '%%x'.
    $pattern = preg_replace('/(%%[a-zA-Z]+)/u', "'$1'", $pattern);

    foreach ($replaces as $strftime => $intl) {
        $pattern = preg_replace('/(?<!%)(' . $strftime . ')/u', $intl, $pattern);
    }// endforeach;

    return $pattern;
  }

	// Format a local time/date according to locale settings
	public static function format($time, $format)
	{

    $pattern = Date::transformformat($format);

    $locale = setlocale(LC_CTYPE, 0);

    $fmt = new IntlDateFormatter(
        $locale,
        IntlDateFormatter::FULL, 
        IntlDateFormatter::FULL,
        null,
        IntlDateFormatter::GREGORIAN
    );

    $fmt->setPattern($pattern);    

//		$date = strftime($format, $time);
    $date = $fmt->format($time);

		return($date);
	}

	// Format a GMT/UTC+0 date/time
	public static function format_gmt($time, $format)
	{
		$date = gmdate($format, $time);

		return( $date );
	}

	public static function atom($time)
	{
		$date = date(DATE_ATOM, $time);

		return( $date );
	}

}

?>
