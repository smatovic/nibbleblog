<?php

/*
 * Nibbleblog -
 * http://www.nibbleblog.com
 * Author Diego Najar

 * All Nibbleblog code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
*/

class NBXML extends SimpleXMLElement
{
	// Private keys = array('username'=>'diego');
	public function addGodChild($name, $private_key)
	{
    $name = mb_convert_encoding($name, "UTF-8", mb_detect_encoding($name));

		// Add and scape &
		$node = parent::addChild($name);
		$node[0] = ''; // (BUG) Con esta forma escapamos el & que no escapa el addChild

		foreach($private_key as $name=>$value)
			$node->addAttribute($name, $value);

		return $node;
	}

#[\ReturnTypeWillChange]
	public function addChild($name, $value='', $namespace='')
	{
		// Get type of the value will be insert
		$type	= gettype($value);

		// Encode to UTF8
    $name = mb_convert_encoding($name, mb_detect_encoding($name));
    $value = mb_convert_encoding($value, "UTF-8", mb_detect_encoding($value));

		// Add and scape &
		$node = parent::addChild($name);
		$node[0] = $value; // (BUG) Con esta forma escapamos el & que no escapa el addChild

		// Add type
		$node->addAttribute('type', $type);

		return $node;
	}

#[\ReturnTypeWillChange]
	public function addAttribute($name, $value='', $namespace='')
	{
    $name = mb_convert_encoding($name, mb_detect_encoding($name));
    $value = mb_convert_encoding($value, "UTF-8", mb_detect_encoding($value));

		return parent::addAttribute($name, $value);
	}

	public function getAttribute($name)
	{
    return mb_convert_encoding((string)$this->attributes()->{$name}, mb_detect_encoding((string)$this->attributes()->{$name}), 'UTF-8');
	}

	public function setChild($name, $value)
	{
		if(isset($this->{$name}))
      $this->{$name} = mb_convert_encoding($value, "UTF-8", mb_detect_encoding($value));

		return false;
	}

	public function getChild($name)
	{
		$type = @$this->{$name}->getAttribute('type');
    $value = mb_convert_encoding((string)$this->{$name}, mb_detect_encoding((string)$this->{$name}), 'UTF-8');


		return empty($type) ? $value : $this->cast($type, $value);
	}

	public function is_set($name)
	{
		return isset($this->{$name});
	}

	public function cast($type, $data)
	{
		if($type=='string')
			return (string) $data;
		elseif(($type=='int') || ($type=='integer'))
			return (int) $data;
		elseif(($type=='bool') || ($type=='boolean'))
			return (bool) $data;
		elseif($type=='float')
			return (float) $data;
		elseif($type=='array')
			return (array) $data;
		elseif($type=='object')
			return (object) $data;

		return $data;
	}

}

?>
